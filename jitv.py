# JitV is based upon JitViewer: https://bitbucket.org/pypy/jitviewer
#
# The MIT License (MIT)
#
# Copyright (c) 2010-2013 The PyPy Development Team
# Copyright (c) 2013 Edd Barrett <vext01@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os, os.path, readline, atexit, sys, argparse
import tempfile, subprocess
from collections import OrderedDict
from cmd import Cmd
from types import MethodType


VERSION = 0.1
TERMWIDTH = 80

try:
    import pypy
except ImportError:
    # if we are running out of the pypy source tree, we know where it is
    sys.path.append(os.path.join(os.path.dirname(sys.executable), "..", ".."))
    try:
        import pypy
    except ImportError:
        print("Cannot import the pypy module, check it is in your PYTHONPATH")
        sys.exit(1)

from rpython.tool.jitlogparser.storage import LoopStorage
from rpython.tool.jitlogparser import parser
from rpython.tool.logparser import extract_category

class Config(object):
    def __init__(self):
        self.use_colour = True
        self.use_wrap = False

    def __str__(self):
        return "use_colour = %s\nuse_wrap = %s" % \
                (self.use_colour, self.use_wrap)
config = Config()

DESCR = """JitV: A Console-based browser for PyPy log files"""

EPILOG = """
Typical usage with no existing log file:

    jitv.py --collect <your script> <arg1> ... <argn>

Typical usage with existing log file:

    jitv.py --log <path to your log file>

where you collected a logfile by setting PYPYLOG, e.g.:

    PYPYLOG=jit-log-opt,jit-backend:<path to your log file> pypy arg1 ... argn

When using an existing logfile, the source code of the logged script must be
in the same location as the logfile.
"""

def failout(msg):
    print("*error: %s" % msg)
    sys.exit(1)

def collect_log(args, interp=sys.executable):
    """ Collect a log file using pypy """

    print("Collecting log with: %s %s" % (interp, ' '.join(args)))

    # create a temp file, possibly racey, but very unlikey
    (fd, path) = tempfile.mkstemp(prefix="jitviewer-")
    os.close(fd) # just the filename we want

    # possibly make this configurable if someone asks...
    os.environ["PYPYLOG"] = "jit-log-opt,jit-backend:%s" % (path, )
    print("Collecting log in '%s'..." % path)
    p = subprocess.Popen([interp] + args, env=os.environ).communicate()
    print("\n")

    # We don't check the return status. The user may want to see traces
    # for a failing program!
    return os.path.abspath(path)

def trunc(s):
    if len(s) > 78:
        s = s[0:10] + "..." + s[-58:]
    return s

# width is the line real estate, usually TERMWIDTH.
# indent is the amount by which every line is indented.
# cindent is the amount *more* long lines are indented.
def wrap(s, width=TERMWIDTH, indent=0, cindent=4):
    if not config.use_wrap: return (" " * indent) + s # wrapping is off
    lines = _wrap(s, width, indent, cindent)
    first = True
    sp = ""
    for l in lines:
        if not first: sp += " " * cindent
        sp += l + "\n"
        first = False
    return sp[:-1] # to strip the last \n

def _wrap(s, width, indent, cindent):
    threshold = width - cindent - indent - 1
    if len(s) > threshold:
        rv = [(" " * indent) + s[0:threshold]]
        rv.extend(_wrap(s[threshold:], width, indent, cindent))
        return rv
    else:
        return [(" " * indent) + s]

def run_cmd_redirected(cmd, storage, args, outfilename):
    with open(outfilename, "w") as out_hndl:
        old_use_colour = config.use_colour
        config.use_colour = False
        old_stdout = sys.stdout
        sys.stdout = out_hndl

        cmd["func"](storage, *args)

        sys.stdout = old_stdout
        config.use_colour = old_use_colour
        print("Output written to '%s'" % outfilename)

class CommandParser(Cmd):
    def __init__(self, loop_storage):
        self.storage = loop_storage
        self.loop_names = loop_storage.loops.keys()
        Cmd.__init__(self)

    def dispatch(self, func_name, args_line, nargs):
        args = args_line.split()
        this_mod = sys.modules[__name__]
        handler = getattr(this_mod, func_name)
        if len(args) != nargs:
            print("%s: wrong number of args: expect %s, got %s" %
                    (cmd_str, cmd_info["nargs"], len(args)))
        else:
            handler(self.storage, *args)

    def _complete_loops_single_arg(self, text, line, start_index, end_index):
        if text:
            return [ l for l in self.loop_names if l.startswith(text) ]
        else:
            return self.loop_names

    complete_p = _complete_loops_single_arg
    complete_pa = _complete_loops_single_arg
    complete_e = _complete_loops_single_arg
    complete_ea = _complete_loops_single_arg

    def do_l(self, line):
        """List loops and bridges"""
        self.dispatch("cmd_list_loops_and_bridges", line, 0)

    def do_ll(self, line):
        """List loops"""
        self.dispatch("cmd_list_loops", line, 0)

    def do_lb(self, line):
        """List bridges."""
        self.dispatch("cmd_list_bridges", line, 0)

    def do_p(self, line):
        """Print a loop."""
        self.dispatch("cmd_print_loop", line, 1)

    def do_pa(self, line):
        """Print a loop with ASM."""
        self.dispatch("cmd_print_loop_asm", line, 1)

    def do_w(self, line):
        """Toggle line wrap."""
        self.dispatch("cmd_toggle_wrap", line, 0)

    def do_c(self, line):
        """Toggle colour."""
        self.dispatch("cmd_toggle_colour", line, 0)

    def do_e(self, line):
        """Open loop in $EDITOR or vi.."""
        self.dispatch("cmd_edit_loop", line, 1)

    def do_ea(self, line):
        """Edit loop (including ASM) in $EDITOR or vi."""
        self.dispatch("cmd_edit_loop_asm", line, 1)

    def do_h(self, line):
        """Same as 'help'"""
        self.do_help(line)

def user_input(storage):
    # User probably wants to see a list of loops to start with
    print("")
    cmd_list_loops(storage)

    parser = CommandParser(storage)
    parser.cmdloop()

def create_loop_dict(loops):
    d = {}
    for loop in loops:
        d[mangle_descr(loop.descr)] = loop
    return d

def mangle_descr(descr):
    if descr.startswith('TargetToken('):
        return descr[len('TargetToken('):-1]
    if descr.startswith('<Guard'):
        return 'bridge-' + str(int(descr[len('<Guard0x'):-1], 16))
    if descr.startswith('<Loop'):
        return 'entry-' + descr[len('<Loop'):-1]
    return descr.replace(" ", '-')

COLOUR_GUARD = '\033[91m'   # red
COLOUR_BRIDGE = '\033[94m'  # blue
COLOUR_LOOP = '\033[92m'    # green
COLOUR_ASM = '\033[90m'     # gray
COLOUR_MERGE_POINT = '\033[95m' # magenta
ANSI_RESET = '\033[0m'
def print_colour(colour, msg, newline=True, fh=sys.stdout):
    if config.use_colour:
        fh.write("%s%s%s" % (colour, msg, ANSI_RESET))
    else:
        fh.write(msg)
    if newline: fh.write("\n")
    fh.flush()

def get_loop_info(loop):
    info = {}
    (start, stop) = loop.comment.find('('), loop.comment.rfind(')')
    info["name"] = loop.comment[start + 1:stop]
    info["count"] = getattr(loop, 'count', '?')
    info["descr"] = mangle_descr(loop.descr)
    return info

def list_loops_or_bridges(iterate_over):
    # Sort by count
    sorted_tuples = sorted(iterate_over.iteritems(),
            key = lambda x : get_loop_info(x[1])["count"],
            reverse=True)
    iterate_over_sorted = OrderedDict(sorted_tuples)

    for (key, loop) in iterate_over_sorted.items():
        # Each loop is an instance of:
        # rpython.jit.tool.oparser_model.ExtendedTreeLoop
        colour = COLOUR_BRIDGE if key.startswith("bridge") else COLOUR_LOOP
        info = get_loop_info(loop)
        print_colour(colour, key + ":")
        print("  name=%s\n  count=%s, descr=%s" % \
            (trunc(info["name"]), info["count"], info["descr"]))

def print_guard(storage, op, loop, fh=sys.stdout):
    print_colour(COLOUR_GUARD, wrap(str(op)), fh=fh)

    # if the guard has a "bridge" then print it
    descr = mangle_descr(op.descr)
    subloop = storage.loops.get(descr, None)
    if subloop is not None:
        op.bridge = descr
        op.count = getattr(subloop, 'count', '?')
        if (hasattr(subloop, 'count') and hasattr(loop, 'count')):
            percentage = (float(subloop.count) / loop.count) * 100
        else:
            percentage = '?'

        # print the bridge
        print_colour(COLOUR_BRIDGE, "%s: Executed %d times (%0.2f%%)" % \
                (descr, subloop.count, percentage), fh=fh)

# ---------------------------------------------------------------------------
# Commands
# ---------------------------------------------------------------------------

def cmd_list_loops_and_bridges(storage): list_loops_or_bridges(storage.loops)
def cmd_list_loops(storage): list_loops_or_bridges(storage.normal_loops)
def cmd_list_bridges(storage): list_loops_or_bridges(storage.bridges)

def cmd_help(storage):
    # arg unused
    print("\nAvailable commands (name/nargs):")
    for (k, v) in CMDS.items():
        print("  %s/%d\t\t%s" % (k, v["nargs"], v["help"]))
    print("\nColour key:")
    print_colour(COLOUR_LOOP, "    normal loops   ", False)
    print_colour(COLOUR_BRIDGE, "bridge loops   ", False)
    print_colour(COLOUR_GUARD, "guards   ", False)
    print_colour(COLOUR_ASM, "assembler  ", False)
    print_colour(COLOUR_MERGE_POINT, "debug_merge_points")
    print("\nPress CTRL+D to exit.\n")

def cmd_print_loop_asm(storage, which):
    cmd_print_loop(storage, which, show_asm=True)

def cmd_edit_loop(storage, which):
    cmd_print_loop(storage, which, in_editor=True)

def cmd_edit_loop_asm(storage, which):
    cmd_print_loop(storage, which, show_asm=True, in_editor=True)

def cmd_print_loop(storage, which, show_asm=False, in_editor=False):
    try:
        theloop = storage.loops[which]
    except KeyError:
        print("Unknown loop or bridge: %s" % which)
        return

    info = get_loop_info(theloop)
    header_colour = COLOUR_GUARD if which.startswith("bridge") else COLOUR_LOOP

    fh = tempfile.NamedTemporaryFile(prefix='jitv-')

    if in_editor:
        old_color_setting = config.use_colour
        config.use_colour = False

    # fetch the assembler code if we have it
    if hasattr(theloop, 'force_asm'): theloop.force_asm()

    print >> fh, TERMWIDTH * "-"
    print_colour(header_colour, which + ":", fh=fh)
    print >> fh, "  count=%d" % info["count"]
    print >> fh, wrap("name=%s" % info["name"], indent=2)
    print >> fh, wrap("descr=%s" % info['descr'], indent=2)
    print >> fh, TERMWIDTH * "-"

    for op in theloop.operations:
        if op.is_guard():
            print_guard(storage, op, theloop, fh=fh)
        elif op.name.startswith("debug_merge_point"):
            print_colour(COLOUR_MERGE_POINT, wrap(str(op)), fh=fh)
        else:
            print >> fh, wrap(str(op))
        if show_asm == True and op.asm is not None:
            for line in op.asm.split("\n"):
                print_colour(COLOUR_ASM, "   " + line, fh=fh)

    if in_editor:
        editor = os.getenv("EDITOR")
        if not editor: editor = "vi"
        subprocess.call([editor, fh.name])
        config.use_colour = old_color_setting
    else:
        subprocess.call(["less", "-R", fh.name])

    fh.close()

def cmd_toggle_wrap(storage):
    lw = config.use_wrap = not config.use_wrap
    x = "on" if lw else "off"
    print("Line wrapping turned %s" % x)

def cmd_toggle_colour(storage):
    c = config.use_colour = not config.use_colour
    x = "on" if c else "off"
    print("Colour turned %s" % x)

# -------------------------------------------------------------------------
# MAIN
# -------------------------------------------------------------------------

if __name__ == "__main__":

    argparser = argparse.ArgumentParser(
            description = DESCR,
            epilog = EPILOG,
            formatter_class=argparse.RawDescriptionHelpFormatter
    )

    argparser.add_argument("-c", "--collect", nargs=argparse.REMAINDER,
            help="collect logfile now", metavar="ARG")
    argparser.add_argument("-i", "--interp", nargs='?',
            help="specify a custom interpreter to use when collecting a log",
            metavar="INTERP")
    argparser.add_argument("-l", "--log", help="specify existing logfile")

    args = argparser.parse_args()

    # by default us the same pypy we are running as to collect logs
    interp = sys.executable if args.interp is None else args.interp

    if args.collect is not None:
        if len(args.collect) < 1:
            failout("please correctly specify invokation to collect log")
        filename = collect_log(args.collect, interp)
        extra_path = os.path.dirname(args.collect[0]) # add dirname of script to extra_path
    elif args.log is not None:
        filename = args.log
        # preserving behaviour before argparse
        # XXX not robust as it could be. Assumes the logfile is in the same
        # dir as the source code, may not be the case.
        extra_path = os.path.dirname(filename)
    else:
        failout("please specify either --log or --collect")

    (log, loops) = parser.import_log(filename, parser.SimpleParser)
    parser.parse_log_counts(extract_category(log, 'jit-backend-count'), loops)
    storage = LoopStorage(extra_path)

    # One dict mapping an id to a loop/bridge
    storage.loops = create_loop_dict(loops)

    # Subsets of the above, not really part of the LoopStorage type, but
    # a convenient place to store them
    storage.normal_loops = {k : v for (k, v)in storage.loops.items() \
        if not v.descr.startswith('bridge')}
    storage.bridges = {k : v for (k, v) in storage.loops.items() if \
        v.descr.startswith('bridge')}

    print("\n\nJitV-%s" % VERSION)
    user_input(storage)

    if args.collect is not None:
        print("The logfile remains at '%s'" % filename)
